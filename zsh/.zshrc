# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	git
	zsh-z #git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z
)

alias j="z"

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Aliases I like
if [ -e ~/.shells.sh ]; then
	source ~/.shells.sh
	# Default text editor
	export EDITOR VISUAL 
	# Go directory
	export GOPATH
	# Pager
	export PAGER
fi

# Default zsh prompt
# ${ret_status} %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)
# My zsh prompt thingy I like
if [ "$PROMPTUSERNAME" = "yes" ]; then
	PS1='${ret_status} %{$fg[green]%}$USER@%M %{$fg[cyan]%}%~%{$reset_color%} $(git_prompt_info)'
else
	PS1='${ret_status} %{$fg[green]%}%M %{$fg[cyan]%}%~%{$reset_color%} $(git_prompt_info)'
fi

# Syntax highlighting (find at https://github.com/zsh-users/zsh-syntax-highlighting)
if [ -e ~/.oh-my-zsh/custom/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
	source ~/.oh-my-zsh/custom/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# z function
if [ -e ~/.zJump/z.sh ]; then
	. ~/.zJump/z.sh
fi

alias zource="source ~/.zshrc"

# Use vim keybinds in command line
bindkey -v
# Keybinds I want that were removed with 'bindkey -v'
# Alt+. Gives last argument of last word
bindkey "^[." insert-last-word
# Ctrl + Left or right arrow works as expected
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
# Shift+tab goes to end of line
bindkey '^[[Z' vi-end-of-line

# Idk man
stty -ixon

# Homebrew in $PATH
if [ -e /home/linuxbrew/.linuxbrew/bin/brew ]; then
	eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi

# Show whether in insert or vim mode in command line
PS1+='${VIMODE}'
#   '$' for normal insert mode
#   a big red 'v' for command mode
function zle-line-init zle-keymap-select {
	DOLLAR='%B%F{green}$%f%b '
	GIANT_I='%B%F{red}V%f%b '
	VIMODE="${${KEYMAP/vicmd/$GIANT_I}/(main|viins)/$DOLLAR}"
	zle reset-prompt
}

if [ ! -z $TMUX ];
then
	TMUX_SESSION=$(tmux display-message -p '#{session_name}')
	if [ "$TMUX_SESSION" = "Secret" ];
	then
		HISTFILE=""
	fi

fi

# Run command I have that just runs command in a subshell and redirects output to /dev/null
compdef run=xargs

# Alias I have that creates a directory and cds to it
compdef mcd=mkdir

zle -N zle-line-init
zle -N zle-keymap-select
