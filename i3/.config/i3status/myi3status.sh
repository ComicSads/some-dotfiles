#!/bin/sh
# Shell script to prepend i3status with more stuff
# Currently it kills colors/formatting so I don't use it
# Apparently wrapper.pl will do it, but it doesn't work

i3status | while :
do
        read -r line
	echo "$line"  "| Brightness:" "$(cat ~/Documents/Programming/Brightness/current)""%" || exit 1
done

