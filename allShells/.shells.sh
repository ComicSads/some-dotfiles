# shellcheck shell=sh

# Extra path stuff
PATH="$HOME/.local/bin:$PATH:/usr/local/bin/nosudo"
export EDITOR=nvim
export VISUAL="$EDITOR"

# Aliases I want for both of my shells
alias cls='clear'
alias code='nvim -c Program'
alias work='nvim $(fzf)'
alias werk='code $(fzf)'
alias svim='nvim -c NoHistory'
alias i3lock='i3lock -i ~/Pictures/For\ Scripts/Lock'
alias restartkde="killall plasmashell && kstart plasmashell"
alias xclip="xclip -select clipboard"
alias wget="wget -c"
alias search="find | grep"
alias tmux="tmux -2"
alias ydl="youtube-dl"
alias tource="tmux source ~/.tmux.conf"
alias handy="HandBrakeCLI"
alias sc="shellcheck -x"
alias tempdir="cd \$(mktemp -d)"
alias cp="cp -r"
alias md="mkdir -p"
alias rd="rmdir -p"
alias lol="lci"
alias sudo="sudo "
alias mv="mv -i"
alias mg="mage"
alias ln="ln -i"
alias mux="tmux a || tmux"
alias todo="todo-txt"
alias motd="cat /var/run/motd.dynamic"
alias https="http --default-scheme=https"
if command -v trash-put >/dev/null; then
	alias rm="trash-put"
fi

if command -v colordiff >/dev/null; then
	alias diff="colordiff -u"
else
	alias diff="diff -u"
fi

mcd() {
	mkdir -p "$@"
	# shellcheck disable=SC2164
	cd "$@"
}

# gopath
export GOPATH=$HOME/Documents/Programming/GoLang

# executable to assembly code
alias unheck="objdump -d"

# Git aliases
alias g='git'
alias ga='git add'
alias gaa='git add --all'
alias gb='git branch'
alias gbD='git branch -D'
alias gbd='git branch -d'
alias gbr='git branch --remote'
alias gc="git commit"
alias 'gc!'='git commit --amend'
alias gcam='git commit -a -m'
alias gcb='git checkout -b'
alias gcd='git checkout develop'
alias gcm='git checkout master'
alias gco='git checkout'
alias gd='git diff'
alias gds='git diff --staged'
alias gf='git fetch'
alias gl="git log"
alias gm='git merge'
alias gp='git push'
alias gpatch='patch -p1'
alias grb='git rebase'
alias gs='git stash'
alias gsa='git stash apply'
alias gss='git status -s'
alias gst='git status'
alias fuk="git checkout -- ."

alias gpsup='git push --set-upstream origin $(git rev-parse --abbrev-ref HEAD)'

# use nless
if [ -n "$(command -v nless)" ]; then
	alias less='nless'
	alias more='nless +G'
	export PAGER='nless'
else
	export PAGER=less
fi

# Check to see if snap got installed again and recreated the ~/snap folder
if [ -e ~/snap ]; then
	echo "Snap folder located"
fi

# Check to see if server shell script exists and run it
if [ -e ~/.server.sh ]; then
	# shellcheck source=server/.server.sh
	. ~/.server.sh
	export GOPATH
	export PROMPTUSERNAME
fi

# Add gopath bin to path here so handle server gopath being different
PATH="$GOPATH/bin:$PATH"

if [ -d "/home/linuxbrew/.linuxbrew/bin" ]; then
	PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"
fi

export PATH
