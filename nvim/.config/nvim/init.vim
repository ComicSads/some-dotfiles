let &packpath = &runtimepath
set clipboard+=unnamedplus 
if has("vms")
	set nobackup		" do not keep a backup file, use versions instead
else
	set backup		" keep a backup file (restore to previous version)
	if has('persistent_undo')
		set undofile	" keep an undo file (undo changes after closing)
	endif
endif

set hlsearch		" always set search highlight on
set autoindent		" always set autoindenting on
source $VIMRUNTIME/plugin/matchit.vim

" Function to show highlighting group of text, to make it easier to customize colors
nmap <leader>p :call <SID>SynStack()<CR>
function! <SID>SynStack()
	if !exists("*synstack")
		return
	endif
	echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" Command to fix go formatting
command Gofix write | execute "!go fmt" | edit

" Set spellcheck
set spell spelllang=en_us
set spellfile=~/.local/share/nvim/site/spell/en.utf-8.add

" Special spellcheck exclusion rules
" Don't highlight urls
syntax match UrlNoSpell '\w\+:\/\/\S\+' contains=@NoSpell
" Don't highlight acronyms
syntax match AcronymNoSpell '\<\(\u\|\d\)\{3,}s\?\>' contains=@NoSpell

" Make it so lines break on spaces
set linebreak

" Unmap ctrl z which just suspends vim
map <C-z> <Nop>

" Y acts as y$
map Y y$

" Set Scroll wheel to undo and redo
set mouse=a
map <ScrollWheelUp> <C-r>
map <ScrollWheelDown> u

" Put backup files inside folders in the working dir, or in specific folders if that's not available.
set backupdir=./.backup/,~/.local/share/nvim/backup//
set directory=./.swp/,./.swap,~/.local/share/nvim/swap//
set undodir=./.undo/,~/.local/share/nvim/undo//

" Option to not set backup files
command NoHistory set nobackup writebackup directory=/tmp undodir=/tmp viewdir=/tmp backupdir=/tmp

" tmux/vim true color
set background=dark

" ... becomes … (for spell checking)
iabbrev ... …

" I have to switch these on very often so I created a command to do it for me
command Program set nospell number

" Copy all lines in file
command Copyall %y+

" Show the difference between the current buffer and the file to be written to
function! s:DiffWithSaved()
	let filetype=&ft
	diffthis
	vnew | read # | normal! 1Gdd
	diffthis
	execute "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
command! BufferDiff call s:DiffWithSaved()

" Rename command (requires vim-dirvish)
" command Rename execute "read !ls" | %Shdo mv -vi -- {} {}
command Rename %Shdo mv -vi -- {} {}

" Line up down works with word wrap
noremap <silent> k gk
noremap <silent> j gj
noremap <silent> $ g$
" Preserve default line up down behavior
noremap <silent> gk k
noremap <silent> gj j
noremap <silent> g$ $

" Make 0 go to the first non-whitespace char in line
noremap <silent> 0 g^
noremap <silent> g0 ^
" Make ^ act as 0 normally does
noremap <silent> ^ g0
noremap <silent> g^ 0

" Map normal mode Q to :q
nnoremap Q :q<CR>

" Create a : command to exit file without saving
command Q q!

" Run latex previewing shell script I created
command Preview w | !preview %

" Make ce and cw act like de and dw
set cpoptions-=_

set textwidth=0

set statusline+=%F%m

" Bind Ctrl+S to Ctrl+A so it doesn't conflict with tmux
nnoremap <C-s> <C-a>

" Exit terminal mode with escape
tnoremap <ESC> <C-\><C-n>

" Plugins

" Plugin directory
call plug#begin('~/.local/share/nvim/plugins')

"Installed plugins
Plug 'vim-scripts/fountain.vim'
Plug 'justinmk/vim-dirvish'
Plug 'flazz/vim-colorschemes'
Plug 'tpope/vim-fugitive'

"End of list, call plugin
call plug#end()

" Color Scheme settings, uses flazz/vim-colorschemes
" Colorscheme name
colorscheme luna-term
" Change the colors of comments and folds so that it's easier to read them
highlight Comment ctermfg=246
highlight Folded ctermfg=23
" Change color of search results
highlight Search ctermbg=14 ctermfg=196
" Change color of "-- INSERT --" and the like
highlight ModeMsg ctermfg=231

" Toggle transparency of background
nnoremap <C-t> :call TransparencyToggle()<cr>

let g:transparency_set = 0

function! TransparencyToggle()
	if g:transparency_set
		let g:transparency_set = 0
		highlight Normal ctermbg=234
	else
		let g:transparency_set = 1
		highlight Normal ctermbg=256
	endif
endfunction

" Detect fountain files
autocmd BufRead,BufNewFile *.fountain set filetype=fountain
autocmd BufRead,BufNewFile *.ft set filetype=fountain
" Custom fountain formatting
" Change how transition looks
highlight fountainTransition ctermfg=gray
highlight fountainTransitionForced ctermfg=gray

nnoremap <Leader>m :!mage test<CR>

inoremap jk <Esc>
inoremap kj <Esc>
