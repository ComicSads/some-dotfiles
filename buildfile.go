// +build mage

package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"bufio"
	"strings"

	"github.com/magefile/mage/mg" // mg contains helpful utility functions, like Deps
)

// Global Variables
// Packager manager
var packager []string
// Backup packager manager
var backpack []string
// Nil String Slice (for returning functions)
var nilSlice []string
// List of programs that couldn't be installed
var failed []string
// Bare mininum needed to work
var tiny = []string{"tmux", "vi", "stow", "curl", "go"}
// Mininum packages needed to work comfortably
var little = []string{"zsh", "neovim", "wget", "xclip"}
// All packages for work
var all = []string{"colordiff", "i3", "vlc", "ffmpeg", "filelight", "duplicity", "brew"}
// Games
var games = []string{"openttd", "steam", "nethack", "lutris"}

func List() error {
	list := tiny
	list = append(list, little...)
	list = append(list, all...)
	list = append(list, games...)
	for i:=0; i<len(list); i++ {
		fmt.Println(list[i])
	}
	return nil
}
func Bare() error {
	mg.Deps(setup)
	installThese(tiny...)
	fmt.Println(failed)
	return nil
}

func Reasonable() error {
	mg.Deps(setup)
	toInstall := append(tiny, little...)
	installThese(toInstall...)
	fmt.Println(failed)
	return nil
}

func Lots() error {
	mg.Deps(setup)
	toInstall := append(tiny, little...)
	toInstall = append(toInstall, all...)
	installThese(toInstall...)
	fmt.Println(failed)
	return nil
}

func Games() error {
	mg.Deps(setup)
	installThese(games...)
	fmt.Println(failed)
	return nil
}

func Everything() error {
	mg.Deps(setup)
	toInstall := append(tiny, little...)
	toInstall = append(toInstall, all...)
	toInstall = append(toInstall, games...)
	installThese(toInstall...)
	fmt.Println(failed)
	return nil
}

// Install programs function
func installThese(s ...string) {
	for i:=0;i<len(s);i++ {
		err := install(s[i])
		check(err)
	}
}

// Install Single Program
func install(s string) error {
	if programInstalled(s) {
		fmt.Printf("Program %s already installed, continuing\n", s)
		return nil
	}
	defer pluginCheck(s)
	install := packager[0]
	args := packager[1:]
	fmt.Println(s)
	args = append(args, s)
	cmd := exec.Command(install, args...)
	err := cmd.Run()
	if err == nil {
		fmt.Printf("Program %s installed successfully!\n", s)
		return nil
	}
	if backpack == nil {
		fmt.Printf("Program %s could not be installed, moving on\n", s)
		failed = append(failed, s)
		return nil
	}
	fmt.Printf("Program %s could not be installed with %s, trying with %s instead.\n", s, install, backpack[0])
	install = backpack[0]
	args = backpack[1:]
	args = append(args, s)
	cmd = exec.Command(install, args...)
	err = cmd.Run()
	if err == nil {
		fmt.Printf("Program %s installed successfully!\n", s)
		return nil
	}
	fmt.Printf("Program %s could not installed, logging", s)
	failed = append(failed, s)
	return nil
}

// Installed plugins if needed
func pluginCheck(s string) error {
	if !programInstalled(s) {
		return nil
	}
	switch s {
	case "neovim":
		install("curl")
		run("curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim")
		run("mkdir -p ~/.local/share/nvim/backup/")
		run("mkdir -p ~/.local/share/nvim/swap/")
		run("mkdir -p ~/.local/share/nvim/undo/")
	case "zsh":
		install("curl")
		run("sh -c \"$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)\"")
	case "tmux":
		run("git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm")
}
return nil
}

// Setup variables
func setup() {
	packager = getPackageManager()
}

// Return how to install program
func getPackageManager() (packager []string) {
	if packager != nil {
		return packager
	}
	switch runtime.GOOS {
	case "linux":
		resp := ask("What do you use to install a program?")
		fullResp := strings.Split(resp, " ")
		if programInstalled("brew") {
			fmt.Println("Brew detected")
			if askYN("Would you like to default to installing with brew?") {
				fmt.Println(strings.Join(fullResp, " "), "set as backup packager manager")
				backpack = fullResp
				return []string{"brew", "install"}
			} else {
				fmt.Println("brew install set as backup package manager")
				backpack[0] = "brew"
				backpack[1] = "install"
				return fullResp
			}
		}
		return fullResp
	case "darwin":
		fmt.Println("Assuming Homebrew since you're on mac")
		if programInstalled("brew") {
			return []string{"brew", "install"}
		} else {
			if askYN("Would you like to install Homebrew?") {
				//install homebrew
				// /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
			} else {
				fmt.Println("Exiting")
				os.Exit(0)
			}
		}
	default:
		return nilSlice
	}
	return nilSlice
}

func programInstalled(program string) bool {
	program = specialCasesForProgramDetector(program)
	cmd := exec.Command("/bin/sh", "-c", "command -v " + program)
	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}

func specialCasesForProgramDetector(s string) string {
	switch s {
	case "neovim":
		return "nvim"
	}
	return s
}

// Prompt for a yes or no response to a question
func askYN(s string) bool {
	resp := ask(s)
	return strings.HasPrefix(resp, "y")
}

// Prompt for a response to a question
func ask(s string) string {
	fmt.Println(s)
	r := bufio.NewReader(os.Stdin)
	resp, err := r.ReadString('\n')
	check(err)
	resp = resp[:len(resp)-1]
	return strings.ToLower(resp)
}

// Error handling function
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Simple run command
func run(s string) error {
	cmd := exec.Command("sh", "-c", s)
	return cmd.Run()
}
